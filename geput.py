#!/bin/python
import sys
import os
from shutil import copyfile
from copy import deepcopy
import datetime

def geput_get(supplied = False):
    """Copy files from a destination over a source file, as specified by a .gpt file, then print the outcome."""
    if supplied:
        found = supplied
    else:
        found = check_gpt()
    if found == None:
        sys.exit("Couldn't find a valid .gpt file to read from. Aborting.")
    else:
        #opens the file and reads it into two lists
        sources, dests = read_to_list(found)
        log.stamp("Getting using {0}".format(found), "verbose")
    for i, source in enumerate(sources):
        dest = dests[i]
        dest = dest[0]
        # check that it exists
        if os.path.exists(dest) == False:
            sys.exit("Couldn't find file {0} to read from. Aborting.".format(dest[0]))
            break
        try:
            copyfile(dest, source)
            print("Copying {0} to {1}".format(dest, source))
        except:
            sys.exit("Hit a snag copying {0} to {1}. Aborting.".format(dest, source))
            break
    print("geput get on {0} complete.".format(found))

def geput_put(supplied = False):
    """Copy files from a source file over a destination file, as specified by a .gpt file, then print the outcome."""
    if supplied:
        found = supplied
    else:
        found = check_gpt()
    if found == None:
        sys.exit("Couldn't find a valid .gpt file to read from. Aborting.")
    else:
        #opens the file and reads it into two lists
        sources, dests = read_to_list(found)
        log.stamp("Putting using {0}".format(found), "verbose")
    for i, source in enumerate(sources):
        # check that it exists
        if os.path.exists(curr_dir + "/" + source) == False:
            sys.exit("Couldn't find source file {0} to read from. Aborting.".format(source))
        for dest in dests[i]:
            try:
                copyfile(source, dest)
                log.stamp("Copying {0} to {1}".format(source, dest), "verbose")
                log.count()
            except:
                log.stamp("Failed to put {0} to {1}. Aborted.".format(), "all")
                sys.exit("Hit a snag copying {0} to {1}. Aborting.".format(source, dest))
                break
    log.stamp("Put on {0} complete.".format(found), "verbose")
    log.stamp("Put {0} files using {1}".format(str(log.report()),found), "terse")

def geput_build():
    """Add a source and destination to the .gpt file, creating a new .gpt if none exists."""
    #which file should be built?
    found = check_gpt()
    if found == None:
        #no file found, make blank lists
        manifest_file = default_file
        sources = []
        dests = [ [] ]
    else:
        #opens the file and reads it into two lists
        manifest_file = found
        sources, dests = read_to_list(manifest_file)
    if argv_less(2):
        geput_panic()
    else:
        try:
        # source already exists
            indecs = sources.index(args_stack[0])
            log.stamp('Added additional destination {1} to source file {0} in manifest file {2}'.format(args_stack[0], args_stack[1], manifest_file), "all")
        except:
            #new source
            sources.append(args_stack[0])
            dests.append([])
            indecs = sources.index(args_stack[0])
            log.stamp('Added source file {0} at destination {1} to manifest file {2}'.format(args_stack[0], args_stack[1], manifest_file), "all")
        finally:
            dests[indecs].append(args_stack[1])
            with open(curr_dir + '/' + manifest_file, 'w+') as manifest:
                for i, source in enumerate(sources):
                    string = source + '='
                    for dest in dests[i]:
                        string = string + dest + '|'
                    manifest.write(string[:-1] + '\n')

def geput_log():
    """Set the log level of the project in <foldername>_geput.log, initialising a log file if none exists."""
    if argv_less(1):
        geput_panic()
    else:
        level = args_stack[0].pop()
        level = level.capitalize()
        file_list = []
        with open.file("foldername_geput.log", 'w+') as file:
            for line in file:
                file_list.append(line)
                file_list[0] = "!!! GEPUT LOG LEVEL:{0} !!!/n".format(level)
        for line in file_list:
            log.enter(line)
        if len(file_list) == 1:
            log.stamp("log created/n", "all")
        log.stamp("log level set to {0}/n".format(level), "all")

def geput_help():
    """Do nothing for now, except for acting as a placeholder for future functionality."""
    pass

def geput_jam():
    """Turn a .gpt file into a .gptp or .gptg file."""
    if argv_less(1):
        geput_panic()
    else:
        direction = args_stack[0]
        if direction == "put":
            suffix = "gptp"
            length = -3
            dir_string = "jam putwards"
            jam_ok = False
        elif direction == "get":
            suffix = "gptg"
            length = -3
            dir_string = "jam getwards"
            jam_ok = False
        elif direction == "unjam":
            suffix = "gpt"
            length = -4
            dir_string = "unjam"
            jam_ok = True
        else:
            geput_panic()
    found = check_gpt(jam_ok, jam_ok)
    if found == None:
        sys.exit("Couldn't find a valid .gpt file to {0}. Aborting.".format(dir_string))
    else:
        try:
            new_file = file[:length] + suffix
            copyfile(file, new_file)
            #Should probably destroy the original .gpt at this point.
            print("Converting {0} to {1}".format(file, new_file))
        except:
            sys.exit("Hit a snag copying {0} to {1}. Aborting.".format(file, new_file))

def geput_force():
    """Call geput_get() or geput_put() on an otherwise incompatible .gptp/.gptg file."""
    if argv_less(1):
        geput_panic()
    else:
        direction = args_stack[0]
        get = False
        put = False
        if direction == "put":
            get = True # this seems counterintuitive because it inverts the usual purpose of check_gpt(). we are in fact putting
            found = check_gpt(get,put,False)
            if found == None:
                sys.exit("Couldn't find a valid .gptg file to put from. Aborting.")
            geput_put(found)
        elif direction == "get":
            put = True # this seems counterintuitive because it inverts the usual purpose of check_gpt(). we are in fact getting
            found = check_gpt(get,put,False)
            if found == None:
                sys.exit("Couldn't find a valid .gptp file to get from. Aborting.")
            geput_get(found)
        else:
            geput_panic()

def geput_panic():
    """Call sys.exit() with an error message."""
    sys.exit("Unrecognised command. Try geput help")

def argv_equals(numb):
    """Return True if length of args_stack is equal to a number or False otherwise."""
    return len(args_stack) == numb

def argv_less(numb):
    """Return True if length of args_stack is less than a number or False otherwise."""
    return len(args_stack) < numb

def argv_more(numb):
    """Return True if length of args_stack exceeds a number or False otherwise."""
    return len(args_stack) > numb

def check_gpt(gptg_ok = False, gptp_ok = False, gpt_ok = True):
    """Return the name of the first .gpt, .gptg or .gptp file found matching the search criteria, or None if no file exists."""
    # from https://stackoverflow.com/a/33400758
    for fname in os.listdir('.'):
        if fname.endswith('.gpt'):
            if gpt_ok:
                return fname
            else:
                pass
        elif fname.endswith('.gptg'):
            if gptg_ok:
                return fname
            else:
                pass
        elif fname.endswith('.gptp'):
            if gptp_ok:
                return fname
            else:
                pass
    else:
        print("no file found")
    return None
    # end stackoverflow

def read_to_list(file):
    """Read file into two lists and return list of the lists."""
    left = []
    right = []
    with open(file, 'r') as manifest:
        for line in manifest:
            source, rightside = line.split("=")
            rightside = rightside.strip()
            dest = rightside.split("|")
            left.append(source.strip())
            right.append(dest)
    return [left, right]

def get_log():
    """look for a log file and return a list containing the path to the file and the log level."""
    dir = os.listdir('.')
    file = ""
    level = ""
    for entry in dir:
        if entry.endswith("_geput.log") == False:
            continue
        file = entry
        with open(file, 'r'):
            for line in file:
                if line.beginswith('!!! GEPUT LOG LEVEL:'):
                    if line[20] == "N":
                        level = ""
                    elif line[20] == "T":
                        level = "terse"
                    elif line[20] == "V":
                        level = "verbose"
                    break
                else:
                    file = ""
                    level = ""
                    break
    res = []
    res.append(file)
    res.append(level)
    return res

class Logger(object):
    self.file_counter = 0
    self.fname = ""
    self.log_level = ""
    def __init__(self):
        results = get_log()
        self.fname = results[0]
        self.log_level = results[1]
        
    def stamp(self, message, msg_level):
        """Write the log message to a logfile of the appropriate level."""
        if self.log_level == "":
            pass
        else:
            if msg_level == self.log_level or msg_level == "all":
                with open(fname, 'a') as file:
                    datestamp = '{:%d-%m-%Y %H:%M:%S} '.format(datetime.datetime.now())
                    file.write(datestamp + message + '/n')
                    
    def count(self):
        """Increment self.file_counter."""
        self.file_counter = self.file_counter + 1
        
    def report(self):
        """Return self.file_counter."""
        return self.file_counter
        
    def enter(self, message):
        """Write a message to the logfile"""
        with open(fname, 'a') as file:
            file.write(message)

curr_dir = os.getcwd()
paths = curr_dir.split("/")
folder_name = paths[-1]
default_file = folder_name + '.gpt'

args_stack = deepcopy(sys.argv)
args_stack.pop(0)
verb = args_stack.pop(0)

opt_list = []

log = Logger()

if verb == "get":
    # copies files from destination
    geput_get()
elif verb == "put":
    geput_put()
elif verb == "build":
    geput_build()
elif verb == "check":
    pass
elif verb == "log":
    pass
elif verb == "help":
    pass
elif verb == "jam":
    geput_jam()
elif verb == "force":
    pass
else:
    geput_panic()
    pass
