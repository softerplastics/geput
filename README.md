# geput

a small python utility for syncing files

DISCLAIMER
This software and its documentation are amateur-hour stuff and, as such, are provided without warranties of absolutely any kind.

FEATURES (in-place)

`geput build <file> <destination>` to build a list of the files you want to keep updated and the locations for them to be sent to.
`geput put` to copy ("clobber") the source files to the destinations you have specified.
`geput get` to copy ("clobber") files at the destination over the source files.

USING GEPUT

geput reads from a special .gpt (or .gptp or .gptg in future versions) file in the same directory as the source files. This file can be built in the shell (using `geput build`) or written in your text editor of choice. The format of a .gpt, .gptp or gptg file is as follows:
```
name_of.file=/absolute/path/to/destination/including/name_of.file
another.file=/absolute/path/to/elsewhere/another.file
```
Note the lack of whitespace around the '=' separator.
To build this in the shell you would use `geput build name_of.file /absolute/path/to/destination/including/name_of.file` and then `geput build another.file /absolute/path/to/elsewhere/another.file`
Optionally, a file can be `geput put` to more than one destination:
```
name_of.file=/path/to/first/name_of.file|/path/to/second/name_of.file|/path/to/third/name_of.file
```
To build this in the shell you would use:
`geput build name_of.file /path/to/first/name_of.file` then
`geput build name_of.file /path/to/second/name_of.file` and finally
`geput build name_of.file /path/to/third/name_of.file`
Only the first destination will be used to clobber the source during `geput get`. Apart from this consideration, ordering of destinations is not important to the operation of geput. geput does not currently support merging or concatenation of multiple destination files.

WORK-IN-PROGRESS FEATURES

Use `geput jam put` or `geput jam get` to lock geput files into only putting or getting in the current folder; this can be overridden with `geput force put` or `geput force get` and undone with `geput unjam`.
Use `geput log verbose` or `geput log terse` to enable logging and `geput log none` to disable it.
Use `geput help` or `geput help <command>` for help in the shell.